package com.example.princebatra.princezomatotest.activity.filter;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.example.princebatra.princezomatotest.R;
import com.example.princebatra.princezomatotest.adapters.filterAdapter.FilterOptionAdapter;
import com.example.princebatra.princezomatotest.callbacks.filtercallbacks.OnFilterOptionClicked;
import com.example.princebatra.princezomatotest.common.Constants;
import com.example.princebatra.princezomatotest.utils.NoSwipeViewPager;
import com.example.princebatra.princezomatotest.databinding.FilterLayoutBinding;
import com.example.princebatra.princezomatotest.fragments.filterfragments.BaseFilterFragment;
import com.example.princebatra.princezomatotest.models.filtermodel.BaseFilterModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;


/**
 * @author princebatra
 * @version 1.0
 * @since 6/6/17
 */
public abstract class BaseFilterActivity extends AppCompatActivity implements View.OnClickListener, OnFilterOptionClicked {

    protected ArrayList<BaseFilterModel> filterOptionMap;
    protected HashMap<String, BaseFilterFragment> fragmentsCollection;
    private RecyclerView filterOptionRv;
    private LinearLayoutManager mLayoutManager;
    private NoSwipeViewPager filterOptionViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme_NoActionBar);
        super.onCreate(savedInstanceState);
        FilterLayoutBinding binder = DataBindingUtil.setContentView(this, R.layout.filter_layout);
        Bundle extras = savedInstanceState == null ? getIntent().getExtras() : savedInstanceState;
        if (extras == null) {
            finish();
            return;
        }

        filterOptionMap = getFilterOptionMap();
        if (filterOptionMap == null || filterOptionMap.size() <= 0) {
            throw new NullPointerException("FilterOptionMap cannot null or empty");
        }

        findViewById(R.id.filter_actionbar_back).setOnClickListener(this);
        binder.filterFooter.setOnClickListener(this);
        findViewById(R.id.resetFilter).setOnClickListener(this);
        filterOptionRv = (RecyclerView) findViewById(R.id.filterOptionRV);

        mLayoutManager = new LinearLayoutManager(this);


        filterOptionViewPager = (NoSwipeViewPager) findViewById(R.id.filter_option_viewpager);
        filterOptionViewPager.setAdapter(new BaseFilterActivity.FilterOptionPagerAdapter(getSupportFragmentManager()));

        setFilterOption();

    }

    private void setFilterOption() {
        filterOptionRv.setLayoutManager(mLayoutManager);
        filterOptionRv.setAdapter(new FilterOptionAdapter(filterOptionMap, this, this));
    }

    @Override
    public void onOptionClicked(int postion) {
        filterOptionViewPager.setCurrentItem(postion, true);
    }

    public abstract ArrayList<BaseFilterModel> getFilterOptionMap();

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.resetFilter:
                Set<String> keys = fragmentsCollection.keySet();
                for (String singleKey : keys) {
                    BaseFilterFragment baseFilterFragment = fragmentsCollection.get(singleKey);
                    if (baseFilterFragment != null) {
                        baseFilterFragment.resetCommandReceived();
                    }
                }
                break;

            case R.id.filter_footer:
                applyFilterClicked();
            case R.id.filter_actionbar_back:
                onBackPressed();
                break;
        }
    }

    private void applyFilterClicked() {
        Intent intent = new Intent();
        intent.putExtra(Constants.FilterModel, filterOptionMap);
        setResult(RESULT_OK, intent);
    }

    private class FilterOptionPagerAdapter extends FragmentPagerAdapter {

        FilterOptionPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

        @Override
        public Fragment getItem(int position) {
            BaseFilterFragment baseFilterFragment = null;
            try {
                Class fragmentClassName = Class.forName(filterOptionMap.get(position).getFragmentClassName());
                baseFilterFragment = (BaseFilterFragment) fragmentClassName.newInstance();
                Bundle bundle = new Bundle();
                bundle.putSerializable(Constants.FilterModel, filterOptionMap.get(position));
                baseFilterFragment.setArguments(bundle);
                if (fragmentsCollection == null) {
                    fragmentsCollection = new HashMap<>();
                }
                fragmentsCollection.put(filterOptionMap.get(position).getLeftSideKey(), baseFilterFragment);
            } catch (Exception e) {
                Log.e("Prince", e.toString());
            }
            return baseFilterFragment;
        }

        @Override
        public int getCount() {
            return filterOptionMap.size();
        }
    }
}