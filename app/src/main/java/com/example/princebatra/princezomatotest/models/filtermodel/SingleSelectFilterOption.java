package com.example.princebatra.princezomatotest.models.filtermodel;

import java.io.Serializable;

/**
 * @author princebatra
 * @version 1.0
 * @since 6/6/17
 */
public class SingleSelectFilterOption implements Serializable {

    private String optionValue;
    private String optionId;
    private boolean isSelected;

    public SingleSelectFilterOption(String _optionValue, boolean _isSelected, String _optionId) {
        this.optionValue = _optionValue;
        this.optionId = _optionId;
        this.isSelected = _isSelected;
    }


    public String getOptionId() {
        return optionId;
    }

    public void setOptionId(String optionId) {
        this.optionId = optionId;
    }

    public String getOptionValue() {
        return optionValue;
    }

    public void setOptionValue(String optionValue) {
        this.optionValue = optionValue;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
