package com.example.princebatra.princezomatotest.dal;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.princebatra.princezomatotest.models.RestaurantsModel;
import com.google.gson.Gson;

import java.util.ArrayList;

/**
 * @author princebatra
 * @version 1.0
 * @since 6/6/17
 */
public class DatabaseHandler extends SQLiteOpenHelper {

    private static final String databaseName = "Restaurants.db";
    private static final String restaurantsTable = "RestaurantsTable";
    private static final String restaurantId = "RestaurantId";
    private static final String restaurantData = "RestaurantData";
    private static int version = 1;


    public DatabaseHandler(Context context) {
        super(context, databaseName, null, version);
    }


    public boolean saveRestaurant(RestaurantsModel.Restaurant singleRestaurant) {
        try {
            long insertId = -1;
            SQLiteDatabase db = getWritableDatabase();

            try {
                db.beginTransaction();
                ContentValues contentValues = new ContentValues();
                contentValues.put(restaurantId, singleRestaurant.getId());
                contentValues.put(restaurantData, new Gson().toJson(singleRestaurant));
                insertId = db.insert(restaurantsTable, null, contentValues);
                db.setTransactionSuccessful();
            } catch (Exception e) {
                Log.e("tag", e.toString());
            }
            db.endTransaction();

            return insertId != -1;
        } catch (Exception e) {
            return false;
        }
    }

    public ArrayList<RestaurantsModel.Restaurant> getRestaurants(String restroId) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor;
        if (restroId != null && restroId.length() > 0) {
            cursor = db.query(restaurantsTable, null, restaurantId + "=" + restroId, null, null, null, null);
        } else {
            cursor = db.query(restaurantsTable, null, null, null, null, null, null);
        }
        if (cursor == null || cursor.getCount() <= 0) {
            return null;
        }
        ArrayList<RestaurantsModel.Restaurant> list = new ArrayList<>();
        while (cursor.moveToNext()) {
            RestaurantsModel.Restaurant singleRestro = new Gson().fromJson(cursor.getString(cursor.getColumnIndex(restaurantData)),
                    RestaurantsModel.Restaurant.class);
            list.add(singleRestro);
        }
        cursor.close();
        return list;
    }

    public boolean deleteRestro(String restroId) {
        SQLiteDatabase db = getWritableDatabase();
        return db.delete(restaurantsTable, restaurantId + "=" + restroId, null) > 0;
    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("Create table  " + restaurantsTable + " ( " + restaurantId + " text not null, " + restaurantData + " text not null )");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
