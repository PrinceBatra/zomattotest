package com.example.princebatra.princezomatotest.adapters;

import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.princebatra.princezomatotest.R;
import com.example.princebatra.princezomatotest.common.SingletonClass;
import com.example.princebatra.princezomatotest.dal.DatabaseHandler;
import com.example.princebatra.princezomatotest.databinding.RestaurantViewBinding;
import com.example.princebatra.princezomatotest.models.RestaurantsModel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @author princebatra
 * @version 1.0
 * @since 6/6/17
 */
public class RestaurantsAdapter extends RecyclerView.Adapter<RestaurantsAdapter.DataObjectHolder> {

    private List<RestaurantsModel.Restaurant> restaurants;
    private AppCompatActivity activity;
    private boolean fromLocal;

    public RestaurantsAdapter(List<RestaurantsModel.Restaurant> _data, AppCompatActivity _activity,
                              boolean _fromLocal) {
        activity = _activity;
        restaurants = _data;
        fromLocal = _fromLocal;
    }


    private float round(float d, int decimalPlace) {
        BigDecimal bd = new BigDecimal(Float.toString(d));
        bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
        return bd.floatValue();
    }

    @Override
    public RestaurantsAdapter.DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        RestaurantViewBinding binder = DataBindingUtil.inflate(layoutInflater, R.layout.restaurant_view, parent, false);
        return new RestaurantsAdapter.DataObjectHolder(binder);
    }

    @Override
    public void onBindViewHolder(final RestaurantsAdapter.DataObjectHolder holder, final int position) {
        final RestaurantsModel.Restaurant restaurant;
        if (fromLocal) {
            restaurant = restaurants.get(position);
        } else {
            restaurant = restaurants.get(position).getRestaurant();
        }
        holder.binder.txtLocationName.setText(restaurant.getName());
        if (restaurant.getLocation() != null) {
            holder.binder.txtLocationAddress.setText(restaurant.getLocation().getAddress());
        }

        String distanceString = SingletonClass.getInstance().getDistance(Double.valueOf(restaurant.getLocation().getLatitude()),
                Double.valueOf(restaurant.getLocation().getLongitude()));

        if (distanceString == null || distanceString.length() <= 0) {
            holder.binder.txtLocationDistance.setVisibility(View.GONE);
        } else {
            holder.binder.txtLocationDistance.setVisibility(View.VISIBLE);
            float distance = Float.valueOf(distanceString);
            if (distance < 1000) {
                holder.binder.txtLocationDistance.setText(String.valueOf(round(distance, 2)) + " meters(Straight  Line)");
            } else {
                distance = distance / 1000;
                holder.binder.txtLocationDistance.setText(String.valueOf(round(distance, 2)) + " KM(Straight  Line)");
            }
        }

        holder.binder.txtCussine.setText(restaurant.getCuisines());


        if (restaurant.getUserRating() == null ||
                restaurant.getUserRating().getAggregateRating() == null
                || restaurant.getUserRating().getAggregateRating().length() <= 0 ||
                restaurant.getUserRating().getRatingText().equalsIgnoreCase("Not rated")) {
            holder.binder.ratingLocation.setVisibility(View.GONE);
            holder.binder.txtRating.setVisibility(View.GONE);
        } else {
            holder.binder.ratingLocation.setVisibility(View.VISIBLE);
            holder.binder.txtRating.setVisibility(View.VISIBLE);
            float rateFloat = Float.valueOf(restaurant.getUserRating().getAggregateRating());
            holder.binder.ratingLocation.setRating(rateFloat);
            holder.binder.txtRating.setText(String.valueOf(holder.binder.ratingLocation.getRating()));
        }
        ArrayList<RestaurantsModel.Restaurant> arrayList = new DatabaseHandler(activity).getRestaurants(restaurant.getId());
        if (arrayList != null && arrayList.size() > 0) {
            holder.binder.txtOffline.setText(activity.getResources().getString(R.string.removeoffline));
        } else {
            holder.binder.txtOffline.setText(activity.getResources().getString(R.string.saveoffline));
        }


        holder.binder.txtAveragePrice.setText("Average Cost: " + restaurant.getAverageCostForTwo() + " Rs.");

        if (restaurant.getIsDeliveringNow() >= 1) {
            holder.binder.txtOpenClose.setText("Open");
            holder.binder.txtOpenClose.setTextColor(Color.BLUE);
        } else {
            holder.binder.txtOpenClose.setText("Closed");
            holder.binder.txtOpenClose.setTextColor(Color.RED);
        }
        Glide.with(activity).load(restaurant.getFeaturedImage())
                .crossFade()
                .centerCrop()
                .placeholder(R.drawable.ic_default_icon)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .error(R.drawable.ic_error)
                .into(holder.binder.imgLocation);


        holder.binder.txtOffline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<RestaurantsModel.Restaurant> arrayList = new DatabaseHandler(activity).getRestaurants(restaurant.getId());
                if (arrayList != null && arrayList.size() > 0) {
                    if (new DatabaseHandler(activity).deleteRestro(arrayList.get(0).getId())) {
                        holder.binder.txtOffline.setText(activity.getResources().getString(R.string.saveoffline));
                        SingletonClass.getInstance().showSnackbar(holder.binder.getRoot(), activity.getResources().getString(R.string.successfullyDeleted));
                        if (fromLocal) {
                            restaurants.remove(position);
                            notifyDataSetChanged();
                        }
                    } else {
                        SingletonClass.getInstance().showSnackbar(holder.binder.getRoot(), activity.getResources().getString(R.string.failedDelete));
                    }
                } else {
                    if (new DatabaseHandler(activity).saveRestaurant(restaurant)) {
                        holder.binder.txtOffline.setText(activity.getResources().getString(R.string.removeoffline));
                        SingletonClass.getInstance().showSnackbar(holder.binder.getRoot(), activity.getResources().getString(R.string.successfullySaved));
                    } else {
                        SingletonClass.getInstance().showSnackbar(holder.binder.getRoot(), activity.getResources().getString(R.string.failedSave));
                    }
                }
            }
        });
    }


    @Override
    public int getItemCount() {
        return restaurants.size();
    }

    class DataObjectHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {

        RestaurantViewBinding binder;

        DataObjectHolder(RestaurantViewBinding _binder) {
            super(_binder.getRoot());
            binder = _binder;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

        }
    }
}

