package com.example.princebatra.princezomatotest.models;



import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author princebatra
 * @version 1.0
 * @since 6/6/17
 */
public class RestaurantsModel {

    @SerializedName("results_found")
    @Expose
    private Integer resultsFound;
    @SerializedName("results_start")
    @Expose
    private String resultsStart;
    @SerializedName("results_shown")
    @Expose
    private Integer resultsShown;
    @SerializedName("restaurants")
    @Expose
    private List<Restaurant> restaurants = null;

    public Integer getResultsFound() {
        return resultsFound;
    }

    public void setResultsFound(Integer resultsFound) {
        this.resultsFound = resultsFound;
    }

    public String getResultsStart() {
        return resultsStart;
    }

    public void setResultsStart(String resultsStart) {
        this.resultsStart = resultsStart;
    }

    public Integer getResultsShown() {
        return resultsShown;
    }

    public void setResultsShown(Integer resultsShown) {
        this.resultsShown = resultsShown;
    }

    public List<Restaurant> getRestaurants() {
        return restaurants;
    }

    public void setRestaurants(List<Restaurant> restaurants) {
        this.restaurants = restaurants;
    }


    public class UserRating {

        @SerializedName("aggregate_rating")
        @Expose
        private String aggregateRating;
        @SerializedName("rating_text")
        @Expose
        private String ratingText;
        @SerializedName("rating_color")
        @Expose
        private String ratingColor;
        @SerializedName("votes")
        @Expose
        private String votes;

        public String getAggregateRating() {
            return aggregateRating;
        }

        public void setAggregateRating(String aggregateRating) {
            this.aggregateRating = aggregateRating;
        }

        public String getRatingText() {
            return ratingText;
        }

        public void setRatingText(String ratingText) {
            this.ratingText = ratingText;
        }

        public String getRatingColor() {
            return ratingColor;
        }

        public void setRatingColor(String ratingColor) {
            this.ratingColor = ratingColor;
        }

        public String getVotes() {
            return votes;
        }

        public void setVotes(String votes) {
            this.votes = votes;
        }

    }

    public class Restaurant {

        @SerializedName("R")
        @Expose
        private R r;
        @SerializedName("apikey")
        @Expose
        private String apikey;
        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("url")
        @Expose
        private String url;
        @SerializedName("location")
        @Expose
        private Location location;
        @SerializedName("switch_to_order_menu")
        @Expose
        private Integer switchToOrderMenu;
        @SerializedName("cuisines")
        @Expose
        private String cuisines;
        @SerializedName("average_cost_for_two")
        @Expose
        private Integer averageCostForTwo;
        @SerializedName("price_range")
        @Expose
        private Integer priceRange;
        @SerializedName("currency")
        @Expose
        private String currency;
        @SerializedName("offers")
        @Expose
        private List<Object> offers = null;
        @SerializedName("thumb")
        @Expose
        private String thumb;
        @SerializedName("user_rating")
        @Expose
        private UserRating userRating;
        @SerializedName("photos_url")
        @Expose
        private String photosUrl;
        @SerializedName("menu_url")
        @Expose
        private String menuUrl;
        @SerializedName("featured_image")
        @Expose
        private String featuredImage;
        @SerializedName("has_online_delivery")
        @Expose
        private Integer hasOnlineDelivery;
        @SerializedName("is_delivering_now")
        @Expose
        private Integer isDeliveringNow;
        @SerializedName("deeplink")
        @Expose
        private String deeplink;
        @SerializedName("has_table_booking")
        @Expose
        private Integer hasTableBooking;
        @SerializedName("events_url")
        @Expose
        private String eventsUrl;
        @SerializedName("establishment_types")
        @Expose
        private List<Object> establishmentTypes = null;
        @SerializedName("order_url")
        @Expose
        private String orderUrl;
        @SerializedName("order_deeplink")
        @Expose
        private String orderDeeplink;
        @SerializedName("book_url")
        @Expose
        private String bookUrl;

        @SerializedName("restaurant")
        @Expose
        private Restaurant restaurant;

        public R getR() {
            return r;
        }

        public void setR(R r) {
            this.r = r;
        }

        public String getApikey() {
            return apikey;
        }

        public void setApikey(String apikey) {
            this.apikey = apikey;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public Location getLocation() {
            return location;
        }

        public void setLocation(Location location) {
            this.location = location;
        }

        public Integer getSwitchToOrderMenu() {
            return switchToOrderMenu;
        }

        public void setSwitchToOrderMenu(Integer switchToOrderMenu) {
            this.switchToOrderMenu = switchToOrderMenu;
        }

        public String getCuisines() {
            return cuisines;
        }

        public void setCuisines(String cuisines) {
            this.cuisines = cuisines;
        }

        public Integer getAverageCostForTwo() {
            return averageCostForTwo;
        }

        public void setAverageCostForTwo(Integer averageCostForTwo) {
            this.averageCostForTwo = averageCostForTwo;
        }

        public Integer getPriceRange() {
            return priceRange;
        }

        public void setPriceRange(Integer priceRange) {
            this.priceRange = priceRange;
        }

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public List<Object> getOffers() {
            return offers;
        }

        public void setOffers(List<Object> offers) {
            this.offers = offers;
        }

        public String getThumb() {
            return thumb;
        }

        public void setThumb(String thumb) {
            this.thumb = thumb;
        }

        public UserRating getUserRating() {
            return userRating;
        }

        public void setUserRating(UserRating userRating) {
            this.userRating = userRating;
        }

        public String getPhotosUrl() {
            return photosUrl;
        }

        public void setPhotosUrl(String photosUrl) {
            this.photosUrl = photosUrl;
        }

        public String getMenuUrl() {
            return menuUrl;
        }

        public void setMenuUrl(String menuUrl) {
            this.menuUrl = menuUrl;
        }

        public String getFeaturedImage() {
            return featuredImage;
        }

        public void setFeaturedImage(String featuredImage) {
            this.featuredImage = featuredImage;
        }

        public Integer getHasOnlineDelivery() {
            return hasOnlineDelivery;
        }

        public void setHasOnlineDelivery(Integer hasOnlineDelivery) {
            this.hasOnlineDelivery = hasOnlineDelivery;
        }

        public Integer getIsDeliveringNow() {
            return isDeliveringNow;
        }

        public void setIsDeliveringNow(Integer isDeliveringNow) {
            this.isDeliveringNow = isDeliveringNow;
        }

        public String getDeeplink() {
            return deeplink;
        }

        public void setDeeplink(String deeplink) {
            this.deeplink = deeplink;
        }

        public Integer getHasTableBooking() {
            return hasTableBooking;
        }

        public void setHasTableBooking(Integer hasTableBooking) {
            this.hasTableBooking = hasTableBooking;
        }

        public String getEventsUrl() {
            return eventsUrl;
        }

        public void setEventsUrl(String eventsUrl) {
            this.eventsUrl = eventsUrl;
        }

        public List<Object> getEstablishmentTypes() {
            return establishmentTypes;
        }

        public void setEstablishmentTypes(List<Object> establishmentTypes) {
            this.establishmentTypes = establishmentTypes;
        }

        public String getOrderUrl() {
            return orderUrl;
        }

        public void setOrderUrl(String orderUrl) {
            this.orderUrl = orderUrl;
        }

        public String getOrderDeeplink() {
            return orderDeeplink;
        }

        public void setOrderDeeplink(String orderDeeplink) {
            this.orderDeeplink = orderDeeplink;
        }

        public String getBookUrl() {
            return bookUrl;
        }

        public void setBookUrl(String bookUrl) {
            this.bookUrl = bookUrl;
        }

        public Restaurant getRestaurant() {
            return restaurant;
        }

        public void setRestaurant(Restaurant restaurant) {
            this.restaurant = restaurant;
        }
    }


    public class R {

        @SerializedName("res_id")
        @Expose
        private Integer resId;

        public Integer getResId() {
            return resId;
        }

        public void setResId(Integer resId) {
            this.resId = resId;
        }

    }

    public class Location {

        @SerializedName("address")
        @Expose
        private String address;
        @SerializedName("locality")
        @Expose
        private String locality;
        @SerializedName("city")
        @Expose
        private String city;
        @SerializedName("city_id")
        @Expose
        private Integer cityId;
        @SerializedName("latitude")
        @Expose
        private String latitude;
        @SerializedName("longitude")
        @Expose
        private String longitude;
        @SerializedName("zipcode")
        @Expose
        private String zipcode;
        @SerializedName("country_id")
        @Expose
        private Integer countryId;
        @SerializedName("locality_verbose")
        @Expose
        private String localityVerbose;

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getLocality() {
            return locality;
        }

        public void setLocality(String locality) {
            this.locality = locality;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public Integer getCityId() {
            return cityId;
        }

        public void setCityId(Integer cityId) {
            this.cityId = cityId;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getZipcode() {
            return zipcode;
        }

        public void setZipcode(String zipcode) {
            this.zipcode = zipcode;
        }

        public Integer getCountryId() {
            return countryId;
        }

        public void setCountryId(Integer countryId) {
            this.countryId = countryId;
        }

        public String getLocalityVerbose() {
            return localityVerbose;
        }

        public void setLocalityVerbose(String localityVerbose) {
            this.localityVerbose = localityVerbose;
        }

    }
}
