package com.example.princebatra.princezomatotest.common;


import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

import com.example.princebatra.princezomatotest.callbacks.OnLocationCallbacks;

public class LocationTracker implements LocationListener {


    private static LocationTracker locationTracker;
    private boolean consistentlyUpdate;
    private SingletonClass globalInstance;
    private Status status;
    private LocationManager locationManager;
    private OnLocationCallbacks locationListener;
    private Location lasLocation;
    private Context context;
    private boolean passedResultToCallback = false;


    private LocationTracker() {
    }

    public static LocationTracker getInstance(Context context) {
        if (locationTracker == null) {
            locationTracker = new LocationTracker();
            locationTracker.context = context;
            locationTracker.status = new Status();
            locationTracker.globalInstance = SingletonClass.getInstance();
        }
        return locationTracker;
    }

    public Status canGetLocation() {
        try {
            Status validationStatus = validateRequirement();
            if (validationStatus.getStatusCode() != Status.StatusCode.SUCCESS) {
                return status;
            }
            locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
            boolean isGpsEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            if (!isGpsEnabled) {
                status.setStatusCode(Status.StatusCode.NO_LOCATION_PROVIDEER_AVAILABLE);
                return status;
            }
            status.setStatusCode(Status.StatusCode.SUCCESS);
            return status;
        } catch (Exception e) {
            status.setStatusCode(Status.StatusCode.EXCEPTION);
            return status;
        }
    }

    public void findLocation(boolean _updateConsistently,int updateTimeInMillis, OnLocationCallbacks _listener) {
        locationListener = _listener;
        consistentlyUpdate = _updateConsistently;
        PackageManager pm = context.getPackageManager();

        if (pm.checkPermission(Manifest.permission.ACCESS_FINE_LOCATION, context.getPackageName()) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        boolean isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        boolean isGpsEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean isPassive = locationManager.isProviderEnabled(LocationManager.PASSIVE_PROVIDER);

        if (isPassive) {
            lasLocation = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
            if (lasLocation != null) {
                status.setStatusCode(Status.StatusCode.LAST_KNOWN_LOCATOIN);
                status.setLocation(lasLocation);
                passedResultToCallback = true;
                locationListener.onComplete(status);
            }
        }
        if (isGpsEnabled) {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, updateTimeInMillis, 0, LocationTracker.this);
        }
        if (isNetworkEnabled) {
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, updateTimeInMillis, 0, LocationTracker.this);
        }
    }

    public void stopFindingLocation() {
        try {
            if (locationManager != null) {
                locationManager.removeUpdates(LocationTracker.this);
            }
        } catch (Exception e) {
            //status.setStatusCode(Status.StatusCode.EXCEPTION);
        }
    }

    private Status validateRequirement() {
        PackageManager pm = context.getPackageManager();
        if (!globalInstance.isNetworkAvailable(context)) {
            status.setStatusCode(Status.StatusCode.NO_INTERNET_CONNECTIVITY_AVAILABLE);
            return status;
        }
        if (pm.checkPermission(Manifest.permission.ACCESS_FINE_LOCATION, context.getPackageName()) != PackageManager.PERMISSION_GRANTED) {
            status.setStatusCode(Status.StatusCode.PEERMISION_ERROR);
            status.setPermissionNeeded(Manifest.permission.ACCESS_FINE_LOCATION);
            return status;
        } else if (pm.checkPermission(Manifest.permission.ACCESS_COARSE_LOCATION, context.getPackageName()) != PackageManager.PERMISSION_GRANTED) {
            status.setStatusCode(Status.StatusCode.PEERMISION_ERROR);
            status.setPermissionNeeded(Manifest.permission.ACCESS_COARSE_LOCATION);
            return status;
        }
        status.setStatusCode(Status.StatusCode.SUCCESS);
        return status;
    }


    @Override
    public void onLocationChanged(Location location) {
        status.setStatusCode(Status.StatusCode.NEW_LOCATION);
        status.setLocation(location);
        SingletonClass.getInstance().setLatLong(context,status.getLatitude(),status.getLongitude());
        if (!consistentlyUpdate) {
            stopFindingLocation();
        }
        if (!passedResultToCallback) {
            passedResultToCallback = true;
            locationListener.onComplete(status);
        }
        if(location.getProvider().equalsIgnoreCase("gps")){
            stopFindingLocation();
        }
        //Toast.makeText(context,location.getProvider(),Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onStatusChanged(String provider, int locationServiceStatus, Bundle extras) {
       /* if (locationServiceStatus == LocationProvider.OUT_OF_SERVICE) {
            startNetworkProviderLocationTracker();
        } else if (locationServiceStatus == LocationProvider.TEMPORARILY_UNAVAILABLE) {
            //Toast.makeText(activity, "Location provider temporarily unavailable.", Toast.LENGTH_SHORT).show();
            status.setStatusCode(Status.StatusCode.WAITING_TO_GET_LOCATOIN);
            status.setLocation(null);
        }*/
    }

    @Override
    public void onProviderEnabled(String provider) {
        Log.e("tag", "came");

    }

    @Override
    public void onProviderDisabled(String provider) {
        Log.e("tag", "came");
    }

}

