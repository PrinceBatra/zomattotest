package com.example.princebatra.princezomatotest.models.filtermodel;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author princebatra
 * @version 1.0
 * @since 6/6/17
 */
public class SingleSelectFilterModel extends BaseFilterModel implements Serializable {

    public ArrayList<SingleSelectFilterOption> filterParams;
    private String key;

    @Override
    public String getLeftSideKey() {
        return key;
    }

    // Overide SingleSelectFilterOption and getFragmentClassName() to make custom MultiselectFilterModel...
    @Override
    public String getFragmentClassName() {
        return "com.example.princebatra.princezomatotest.fragments.filterfragments.SingleSelectionFilterFragment";
    }

    public void setKey(String key) {
        this.key = key;
    }

    public ArrayList<SingleSelectFilterOption> getFilterParams() {
        return filterParams;
    }

    public void setFilterParams(ArrayList<SingleSelectFilterOption> filterParams) {
        this.filterParams = filterParams;
    }
}
