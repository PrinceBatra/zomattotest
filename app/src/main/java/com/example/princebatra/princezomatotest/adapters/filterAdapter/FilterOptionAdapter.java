package com.example.princebatra.princezomatotest.adapters.filterAdapter;

import android.databinding.DataBindingUtil;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.princebatra.princezomatotest.R;
import com.example.princebatra.princezomatotest.callbacks.filtercallbacks.OnFilterOptionClicked;
import com.example.princebatra.princezomatotest.databinding.ListFilterOptionRowBinding;
import com.example.princebatra.princezomatotest.models.filtermodel.BaseFilterModel;

import java.util.ArrayList;

/**
 * @author princebatra
 * @version 1.0
 * @since 6/6/17
 */
public class FilterOptionAdapter extends RecyclerView.Adapter<FilterOptionAdapter.DataObjectHolder> {

    private AppCompatActivity activity;
    private ArrayList<BaseFilterModel> data;
    private OnFilterOptionClicked _listener;
    private View previousView;

    public FilterOptionAdapter(ArrayList<BaseFilterModel> _data, AppCompatActivity _activity, OnFilterOptionClicked listener) {
        activity = _activity;
        data = _data;
        _listener = listener;
    }


    @Override
    public FilterOptionAdapter.DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        ListFilterOptionRowBinding binder = DataBindingUtil.inflate(layoutInflater, R.layout.list_filter_option_row, parent, false);
        return new FilterOptionAdapter.DataObjectHolder(binder);
    }

    @Override
    public void onBindViewHolder(FilterOptionAdapter.DataObjectHolder holder, int position) {

        if (position == 0) {
            holder.itemView.setBackgroundColor(ContextCompat.getColor(activity, R.color.white));
            holder.binder.data.setTextColor(ContextCompat.getColor(activity, R.color.gray));
            previousView = holder.itemView;
        } else {
            holder.binder.data.setTextColor(ContextCompat.getColor(activity, R.color.white));
        }
        holder.binder.data.setText(data.get(position).getLeftSideKey());
    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    class DataObjectHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {

        ListFilterOptionRowBinding binder;

        DataObjectHolder(ListFilterOptionRowBinding _binder) {
            super(_binder.getRoot());
            binder = _binder;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (previousView != null) {
                previousView.setBackgroundColor(ContextCompat.getColor(activity, R.color.gray));
                ListFilterOptionRowBinding previousBinder = DataBindingUtil.getBinding(previousView);
                previousBinder.data.setTextColor(ContextCompat.getColor(activity, R.color.white));
            }
            view.setBackgroundColor(ContextCompat.getColor(activity, R.color.white));
            binder.data.setTextColor(ContextCompat.getColor(activity, R.color.gray));
            _listener.onOptionClicked(getAdapterPosition());
            previousView = view;
        }
    }
}

