package com.example.princebatra.princezomatotest.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import com.example.princebatra.princezomatotest.R;
import com.example.princebatra.princezomatotest.callbacks.ISingleButtonDialogListener;
import com.example.princebatra.princezomatotest.callbacks.ITwoButtonDialogListener;
import com.example.princebatra.princezomatotest.callbacks.OnLocationCallbacks;
import com.example.princebatra.princezomatotest.common.CustomDialog;
import com.example.princebatra.princezomatotest.common.LocationTracker;
import com.example.princebatra.princezomatotest.common.SingletonClass;
import com.example.princebatra.princezomatotest.common.Status;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;

public class SplashActivity extends AppCompatActivity {

    private int REQUEST_CHECK_SETTINGS = 10;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DataBindingUtil.setContentView(this, R.layout.activity_splash);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                checkForLocationAndMoveFurther();
            }
        }, 3000);

    }

    private void askForPermissions(Activity activity, String permission) {
        try {
            if (activity != null) {
                ActivityCompat.requestPermissions(activity, new String[]{permission}, 1);
            }

        } catch (Exception e) {
            Log.e("tag", e.toString());
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    checkForLocationAndMoveFurther();
                } else {
                    finish();
                }

            }
        }
    }


    private void displayLocationSettingsRequest(Context context) {
        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API).build();
        googleApiClient.connect();

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(10000 / 2);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(@NonNull LocationSettingsResult result) {
                final com.google.android.gms.common.api.Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        checkForLocationAndMoveFurther();
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            // Show the dialog by calling startResolutionForResult(), and check the result
                            // in onActivityResult().
                            status.startResolutionForResult(SplashActivity.this, REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            showSettingPageDialogue();
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        showSettingPageDialogue();
                        break;
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CHECK_SETTINGS && resultCode == RESULT_OK) {
            checkForLocationAndMoveFurther();
        } else {
            finish();
        }
    }

    private void showSettingPageDialogue() {
        CustomDialog dialog = new CustomDialog(SplashActivity.this, 0);
        dialog.displayTwoButtonsDialog(getResources().getString(R.string.setting_device_location_permission), "Ok", "Cancel", new ITwoButtonDialogListener() {
            @Override
            public void onPositiveClickListener(CustomDialog dialogInstance) {
                Intent viewIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(viewIntent);
                finish();
            }

            @Override
            public void onNegativeClickListener(CustomDialog dialogInstance) {
                finish();
            }
        });
    }

    private void checkForLocationAndMoveFurther() {
        try {
            LocationTracker locationTracker = LocationTracker.getInstance(getApplicationContext());
            Status status = locationTracker.canGetLocation();

            if (status.getStatusCode() == Status.StatusCode.SUCCESS) {
                locationTracker.findLocation(false, 60000, new OnLocationCallbacks() {
                    @Override
                    public void onComplete(Status status) {
                        if (status != null) {
                            SingletonClass.getInstance().setLatLong(null, status.getLatitude(), status.getLongitude());
                            Intent intent = new Intent(SplashActivity.this, HomePhoneActivity.class);
                            startActivity(intent);
                            finish();
                        } else {
                            CustomDialog dialog = new CustomDialog(SplashActivity.this, 0);
                            dialog.displaySingleButtonDailog(getResources().getString(R.string.failed_to_find_current_location), "Ok", new ISingleButtonDialogListener() {
                                @Override
                                public void onPositiveClickListener(CustomDialog dialogInstance) {
                                    finish();
                                }
                            });
                        }
                    }
                });
            } else if (status.getStatusCode() == Status.StatusCode.PEERMISION_ERROR) {
                askForPermissions(this, status.getPermissionNeeded());
            } else if (status.getStatusCode() == Status.StatusCode.NO_LOCATION_PROVIDEER_AVAILABLE) {
                displayLocationSettingsRequest(SplashActivity.this);
            } else {
                CustomDialog dialog = new CustomDialog(SplashActivity.this, 0);
                dialog.displaySingleButtonDailog(status.getStatusCode().toString(), "Ok", new ISingleButtonDialogListener() {
                    @Override
                    public void onPositiveClickListener(CustomDialog dialogInstance) {
                        finish();
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        LocationTracker.getInstance(this).stopFindingLocation();
    }
}
