package com.example.princebatra.princezomatotest.models.filtermodel;

import java.io.Serializable;


/**
 * @author princebatra
 * @version 1.0
 * @since 6/6/17
 */
public abstract class BaseFilterModel implements Serializable{

    public abstract String getLeftSideKey();

    public abstract String getFragmentClassName();

}
