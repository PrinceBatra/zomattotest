package com.example.princebatra.princezomatotest.common;

import android.location.Location;

/**
 * Created by Prince Batra.
 */
public class Status {


    private  StatusCode code;
    private Location location;
    private String permissionNeeded;


    public  StatusCode getStatusCode(){
        return  code;
    }

    protected  void setStatusCode(StatusCode _code){
        this.code = _code;
    }


    protected  void setLocation(Location _location){
        this.location = _location;
    }

    public Location getLocation(){
        return  location;
    }


    public  double getLatitude(){
        if (location == null){
            return -500;
        }
        return  location.getLatitude();
    }

    public  double getLongitude(){
        if (location == null){
            return -500;
        }
        return  location.getLongitude();
    }

    public String getPermissionNeeded() {
        return permissionNeeded;
    }

    public void setPermissionNeeded(String permissionNeeded) {
        this.permissionNeeded = permissionNeeded;
    }


    public enum StatusCode {

        SUCCESS,
        EXCEPTION,
        PASSIVE,
        NO_NETWORK_PROVIDER_AVAILABLE,
        NO_LOCATION_PROVIDEER_AVAILABLE,
        PROVIDER_GET_DISALED,
        WAITING_TO_GET_LOCATOIN,
        LAST_KNOWN_LOCATOIN,
        NEW_LOCATION,
        PEERMISION_ERROR,
        PROVIDER_IS_OUT_OF_SERVICE,
        NO_ACCESS_FINE_LOCATION_PERMISSION_GRANDTED,
        NO_ACCESS_COARSE_LOCATION_PERMISSION_GRANDTED,
        NO_INTERNET_PERMISSION_GRANDTED,
        NO_INTERNET_CONNECTIVITY_AVAILABLE,
        SERVICE_AVAILABLE_FROM_GPS,
        SERVICE_AVAILABLE_FROM_NETWORK_POVIDER
    }

}
