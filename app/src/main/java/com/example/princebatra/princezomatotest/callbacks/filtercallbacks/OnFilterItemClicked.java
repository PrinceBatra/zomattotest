package com.example.princebatra.princezomatotest.callbacks.filtercallbacks;

/**
 * @author princebatra
 * @version 1.0
 * @since 6/6/17
 */
public interface OnFilterItemClicked {
    void onFilterOptionClicked(String key);

    void onFilterValueClicked(String dataKey);
}
