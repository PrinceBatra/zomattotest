package com.example.princebatra.princezomatotest.common;

/**
 * @author princebatra
 * @version 1.0
 * @since 6/6/17
 */
public class Constants {
    public static final int success = 200;
    public static final int exception = 2;
    public static final int failureFromWebService = 3;
    public static final String serverCommunicationFailed = "Failed to connect server.Please try again later.";
    public static final String serverReadingResponseFailed = "Failed to read server response.Please try again later.";
    public static final String serverRunningError = "Check your internet connectivity.";
    public static final String timeout = "Server connection time-out. Please try again";
    public static final String Restaurants = "Restaurants";

    public static final String city_id = "city_id";
    public static final String start = "start";
    public static final String count = "count";
    public static final String lat = "lat";
    public static final String lon = "lon";
    public static final String radius = "radius";
    public static final int maxCount = 20;
    public static final String fromWHere = "fromWHere";
    public static String FilterModel = "FilterModel";
    public static String RestaurantRefreshFilter = "RestaurantRefreshFilter";
    public static String cuisinesIds = "cuisines";
    public static String sort = "sort";
    public static String order = "order";
    public static String query = "q";
    public static String filterId = "filterId";
    public static String seaerchId = "seaerchId";
    public static String scrollmoreId = "scrollmoreId";

}
