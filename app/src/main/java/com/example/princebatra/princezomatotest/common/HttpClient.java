package com.example.princebatra.princezomatotest.common;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;
import com.example.princebatra.princezomatotest.callbacks.IHttpPostResult;
import com.example.princebatra.princezomatotest.callbacks.ISingleButtonDialogListener;
import com.example.princebatra.princezomatotest.callbacks.ITwoButtonDialogListener;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.InterruptedIOException;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.UnknownHostException;


public class HttpClient {

    private String _url;
    private Context _context;
    private String _jsonPacket;
    private IHttpPostResult _httpCallBack;
    private boolean _blockUi;
    private Activity act;
    private StartPosting startPosting;
    private CustomDialog uiBlockingDialog;


    /**
     * context here requires acitvity instance
     */
    public void post(String url, Context activity, String jsonPacket,
                     boolean blockUi, IHttpPostResult httpCallback) {
        try {
            _url = url;
            _context = activity;
            act = (Activity) activity;
            _jsonPacket = jsonPacket;
            _httpCallBack = httpCallback;
            _blockUi = blockUi;

            if (!isNetworkAvailable(act)) {
                noInternetAlert(act);
                return;
            }
            startPosting = new StartPosting();
            startPosting.execute();
        } catch (Exception ignore) {
        }
    }


    public void cancelPreviousRequest() {
        try {
            if (startPosting != null) {
                startPosting.cancel(true);
            }
        } catch (Exception e) {
        }
    }


    public JSONObject postOnMainThread(String url, Context context, String jsonPacket) {

        try {
            if (jsonPacket == null) {
                return null;
            }
            _url = url;
            _context = context;
            _jsonPacket = jsonPacket;

            if (!isNetworkAvailable(context)) {
                return null;
            }
            PostingResult result = new StartPosting().doInBackground();
            if (result.jsonData == null && result.resultCode != Constants.success) {
                return null;
            } else if (result.jsonData != null && result.resultCode == Constants.failureFromWebService) {
                return null;
            }
            return result.jsonData;
        } catch (Exception e) {
            return null;
        }
    }

    private boolean isJSONValid(String test) {
        try {
            if (test == null || test.length() <= 0) {
                return false;
            }
            new JSONObject(test);
        } catch (JSONException ex) {
            try {
                new JSONArray(test);
            } catch (JSONException ex1) {
                return false;
            } catch (Exception e) {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    /**
     * Need to change this method according to service response....
     */
    private void showWebServiceFailureMessage(JSONObject jsonData) {

        try {
            String msg = "";
            msg = jsonData.getString("message");

            CustomDialog dialog = new CustomDialog(_context, 0);

            dialog.displaySingleButtonDailog(msg, "Ok", new ISingleButtonDialogListener() {

                @Override
                public void onPositiveClickListener(CustomDialog dialogInstance) {
                    dialogInstance.dismiss();
                }
            });
        } catch (Exception ignore) {}

    }

    private void noInternetAlert(final Activity activity) {
        try {
            CustomDialog dialog = new CustomDialog(activity, 0);
            dialog.displayTwoButtonsDialog("No internet available.Do you want to retry?", "Retry", "Cancel", new ITwoButtonDialogListener() {

                @Override
                public void onPositiveClickListener(CustomDialog dialogInstance) {
                    dialogInstance.dismiss();
                    post(_url, act, _jsonPacket, _blockUi, _httpCallBack);
                }

                @Override
                public void onNegativeClickListener(CustomDialog dialogInstance) {
                    dialogInstance.dismiss();
                }
            });
        } catch (Exception e) {
        }

    }

    private boolean isNetworkAvailable(Context act) {
        try {
            if (act == null) {
                return false;
            }
            ConnectivityManager connectivity = (ConnectivityManager) act.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (connectivity != null) {
                NetworkInfo activeNetwork = connectivity.getActiveNetworkInfo();
                if (activeNetwork == null) {
                    return false;
                } else {
                    return activeNetwork.getType() == ConnectivityManager.TYPE_WIFI || activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE;
                }
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }

    }

    private class StartPosting extends AsyncTask<Void, Void, PostingResult> {

        @Override
        protected void onPreExecute() {
            uiBlockingDialog = new CustomDialog(_context, 0);
            if (_blockUi) {
                uiBlockingDialog.displayUiBlockingDialog();
            }
        }


        @Override
        protected PostingResult doInBackground(Void... params) {

            HttpURLConnection urlConnection = null;
            try {
                URL urlToRequest = new URL(_url);
                urlConnection = (HttpURLConnection) urlToRequest.openConnection();
                urlConnection.setConnectTimeout(30000);
                urlConnection.setReadTimeout(30000);
                urlConnection.setDoOutput(true);
                urlConnection.setDoInput(true);
                urlConnection.setRequestProperty("user-key","e57b29d89b819551d70214486a6b5d27");
                urlConnection.setRequestProperty("Content-Type", "application/json");
                //urlConnection.setRequestProperty("Accept", "application/json");

                if (_jsonPacket != null) {
                    OutputStreamWriter wr = new OutputStreamWriter(urlConnection.getOutputStream());
                    wr.write(_jsonPacket);
                    wr.flush();
                }

                int statusCode = urlConnection.getResponseCode();
                if (isCancelled()) {
                    return null;
                }

                JSONObject job;
                if (statusCode != HttpURLConnection.HTTP_OK) {
                    InputStream in = new BufferedInputStream(urlConnection.getErrorStream());
                    String responseString = getResponseString(in);
                    if (responseString == null) {
                        return null;
                    }
                    if (isJSONValid(responseString)) {
                        job = new JSONObject(responseString);
                        return new PostingResult(job, Constants.failureFromWebService, "");
                    } else {
                        return new PostingResult(null, statusCode, Constants.serverCommunicationFailed + "Response code = " + statusCode);
                    }

                } else {
                    InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                    String responseString = getResponseString(in);
                    if (responseString == null) {
                        return null;
                    }
                    if (isJSONValid(responseString)) {
                        job = new JSONObject(responseString);
                        return new PostingResult(job, Constants.success, "");
                    } else {
                        return new PostingResult(null, statusCode, Constants.serverCommunicationFailed + Constants.serverReadingResponseFailed);
                    }
                }
            } catch (MalformedURLException e) {
                return new PostingResult(null, Constants.exception, Constants.serverCommunicationFailed + "Invalid URL");
            } catch (SocketTimeoutException e) {
                return new PostingResult(null, Constants.exception, Constants.timeout);
            } catch (UnknownHostException e) {
                return new PostingResult(null, Constants.exception, Constants.serverRunningError);
            } catch (IOException e) {
                return new PostingResult(null, Constants.exception, Constants.serverCommunicationFailed + "Please try again");

            } catch (JSONException e) {
                return new PostingResult(null, Constants.exception, Constants.serverReadingResponseFailed);
            } catch (Exception e) {
                return new PostingResult(null, Constants.exception, Constants.serverCommunicationFailed);
            } finally {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
            }
        }

        @Override
        protected void onPostExecute(PostingResult result) {
            try {
                if (_blockUi) {
                    uiBlockingDialog.stopUiBlocking();
                }
                if (result == null) {
                    showCommunicationFailedAlert(Constants.serverCommunicationFailed);
                    return;
                }
                if (result.jsonData == null && result.resultCode != Constants.success) {
                    showCommunicationFailedAlert(result.errMessage);
                    return;
                } else if (result.jsonData != null && result.resultCode == Constants.failureFromWebService) {
                    showWebServiceFailureMessage(result.jsonData);
                    return;
                }
                _httpCallBack.onSuccess(result.jsonData);
            } catch (Exception ignore) {

            }
        }


        private void showCommunicationFailedAlert(String message) {

            try {
                CustomDialog dialog = new CustomDialog(_context, 0);
                dialog.displayTwoButtonsDialog(message, "Retry", "Cancel", new ITwoButtonDialogListener() {

                    @Override
                    public void onPositiveClickListener(CustomDialog dialogInstance) {

                        dialogInstance.dismiss();
                        post(_url, act, _jsonPacket, _blockUi, _httpCallBack);
                    }

                    @Override
                    public void onNegativeClickListener(CustomDialog dialogInstance) {
                        dialogInstance.dismiss();
                    }
                });
            } catch (Exception e) {
            }
        }


        private String getResponseString(InputStream inStream) {
            try {
                if (isCancelled()) {
                    Log.e("tag", "thread cancelled");
                    return null;
                }
                BufferedReader r = new BufferedReader(new InputStreamReader(inStream));
                StringBuilder total = new StringBuilder();
                String line;
                while ((line = r.readLine()) != null) {
                    total.append(line);
                }
                return total.toString();
            } catch (InterruptedIOException e) {
                return null;
            } catch (Exception e) {
                return null;
            }

        }

    }

    private class PostingResult {

        JSONObject jsonData;
        int resultCode;
        String errMessage;

        protected PostingResult(JSONObject job, int code, String err) {
            this.jsonData = job;
            this.resultCode = code;
            this.errMessage = err;
        }

    }

}
