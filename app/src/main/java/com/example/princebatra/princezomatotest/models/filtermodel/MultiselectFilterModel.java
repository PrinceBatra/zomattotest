package com.example.princebatra.princezomatotest.models.filtermodel;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author princebatra
 * @version 1.0
 * @since 6/6/17
 */
public class MultiselectFilterModel extends BaseFilterModel implements Serializable {

    public ArrayList<MultiSelectFilterOption> filterParams;
    private String key;

    @Override
    public String getLeftSideKey() {
        return key;
    }

    // Overide MultiselectFilterModel and getFragmentClassName() to make custom MultiselectFilterModel...
    @Override
    public String getFragmentClassName() {
        return "com.example.princebatra.princezomatotest.fragments.filterfragments.MultiselectFilterFragment";
    }

    public void setKey(String key) {
        this.key = key;
    }

    public ArrayList<MultiSelectFilterOption> getFilterParams() {
        return filterParams;
    }

    public void setFilterParams(ArrayList<MultiSelectFilterOption> filterParams) {
        this.filterParams = filterParams;
    }
}