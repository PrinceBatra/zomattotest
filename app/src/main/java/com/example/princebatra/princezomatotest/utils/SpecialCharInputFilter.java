package com.example.princebatra.princezomatotest.utils;

import android.support.annotation.IntDef;
import android.text.InputFilter;
import android.text.Spanned;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;


/**
 * @author princebatra
 * @version 1.0
 * @since 6/6/17
 */
public class SpecialCharInputFilter implements InputFilter {
    final public static int ALPHABETS_ONLY = 0;
    final public static int NUMBERS_ONLY = 1;
    final public static int ALPHANUMERIC = 2;
    private String regex;

    public SpecialCharInputFilter(@Characters int range, String exceptions) {
        switch (range) {
            case ALPHABETS_ONLY:
                regex = "^[\\p{L}";
                break;
            case NUMBERS_ONLY:
                regex = "^[\\p{N}";
                break;

            case ALPHANUMERIC:
                regex = "^[\\p{L}\\p{N}";
                break;
        }
        regex += exceptions + "]+$";
    }

    public SpecialCharInputFilter(@Characters int range) {
        this(range, "");
    }

    @Override
    public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
        for (int i = start; i < end; i++) {
            String charAt = String.valueOf(source.charAt(i));
            if (!charAt.matches(regex)) {
                return source.toString().substring(0, source.length() - 1);
            }
        }
        return null;
    }

    @IntDef({ALPHABETS_ONLY, NUMBERS_ONLY, ALPHANUMERIC})
    @Retention(RetentionPolicy.SOURCE)
    public @interface Characters {
    }
}
