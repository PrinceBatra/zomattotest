package com.example.princebatra.princezomatotest.fragments.filterfragments;

import android.support.v4.app.Fragment;

import com.example.princebatra.princezomatotest.models.filtermodel.BaseFilterModel;

import java.io.Serializable;


/**
 * @author princebatra
 * @version 1.0
 * @since 6/6/17
 */
public abstract class BaseFilterFragment extends Fragment implements Serializable{


    public abstract BaseFilterModel getModel();

    public abstract void resetCommandReceived();

}
