package com.example.princebatra.princezomatotest.utils;

import android.graphics.Canvas;
import android.graphics.Paint;

import uk.co.deanwild.materialshowcaseview.shape.Shape;
import uk.co.deanwild.materialshowcaseview.target.Target;

;

/**
 * @author princebatra
 * @version 1.0
 * @since 6/6/17
 */
public class CustomCircle implements Shape {

    private int radius;

    public CustomCircle(int radius) {
        this.radius = radius;
        this.radius = radius;
    }

    public int getRadius() {
        return this.radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public void draw(Canvas canvas, Paint paint, int x, int y, int padding) {
        if (this.radius > 0) {
            canvas.drawCircle((float) x, (float) y, (float) (this.radius + padding), paint);
        }

    }

    public void updateTarget(Target target) {

    }

    public int getWidth() {
        return this.radius * 2;
    }

    public int getHeight() {
        return this.radius * 2;
    }

}
