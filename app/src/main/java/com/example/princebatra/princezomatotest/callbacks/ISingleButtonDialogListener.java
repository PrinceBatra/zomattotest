package com.example.princebatra.princezomatotest.callbacks;


import com.example.princebatra.princezomatotest.common.CustomDialog;

public interface ISingleButtonDialogListener {

	void onPositiveClickListener(CustomDialog dialogInstance);
	
}
