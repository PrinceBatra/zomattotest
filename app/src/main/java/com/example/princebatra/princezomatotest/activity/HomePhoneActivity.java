package com.example.princebatra.princezomatotest.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.example.princebatra.princezomatotest.R;
import com.example.princebatra.princezomatotest.adapters.RestaurantPagerAdapter;
import com.example.princebatra.princezomatotest.callbacks.IHttpPostResult;
import com.example.princebatra.princezomatotest.common.Constants;
import com.example.princebatra.princezomatotest.fragments.RestaurantsList;
import com.example.princebatra.princezomatotest.utils.CustomHashmap;
import com.example.princebatra.princezomatotest.common.HttpClient;
import com.example.princebatra.princezomatotest.common.LocationTracker;
import com.example.princebatra.princezomatotest.common.SingletonClass;
import com.example.princebatra.princezomatotest.databinding.ActivityHomePhoneBinding;
import com.example.princebatra.princezomatotest.models.CuisinesModel;
import com.example.princebatra.princezomatotest.models.filtermodel.BaseFilterModel;
import com.example.princebatra.princezomatotest.models.filtermodel.CustomModel;
import com.example.princebatra.princezomatotest.models.filtermodel.MultiSelectFilterOption;
import com.example.princebatra.princezomatotest.models.filtermodel.MultiselectFilterModel;
import com.example.princebatra.princezomatotest.models.filtermodel.SingleSelectFilterModel;
import com.example.princebatra.princezomatotest.models.filtermodel.SingleSelectFilterOption;
import com.example.princebatra.princezomatotest.utils.SpecialCharInputFilter;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;

import uk.co.deanwild.materialshowcaseview.IShowcaseListener;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseView;

public class HomePhoneActivity extends AppCompatActivity implements View.OnClickListener {

    private ActivityHomePhoneBinding binder;
    private RestaurantPagerAdapter restaurantPagerAdapter;
    private CustomHashmap<String, String> filterRequest;
    private static final int FILTER_ACTION = 101;
    private ArrayList<BaseFilterModel> filterModels;
    private boolean searchEdtVisible = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binder = DataBindingUtil.setContentView(this, R.layout.activity_home_phone);
        getSupportActionBar().hide();
        binder.imgGlobalSearch.setOnClickListener(this);
        binder.imgFilter.setOnClickListener(this);
        binder.searchField.setFilters(new InputFilter[]{new SpecialCharInputFilter(SpecialCharInputFilter.ALPHANUMERIC, "")});

        binder.searchField.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {

                    ArrayList<BaseFilterModel> baseFilterModels = new ArrayList<>();
                    CustomModel customModel = new CustomModel();
                    customModel.setFilterText(binder.searchField.getText().toString());

                    baseFilterModels.add(customModel);
                    Intent intent = new Intent();
                    intent.putExtra(Constants.FilterModel, baseFilterModels);
                    intent.setAction(Constants.RestaurantRefreshFilter);
                    LocalBroadcastManager.getInstance(HomePhoneActivity.this).sendBroadcast(intent);
                    SingletonClass.getInstance().hideKeyboard(binder.searchField);
                    return true;
                }
                return false;
            }
        });

        setViews();
        showHelpScreen();
    }

    private void showHelpScreen() {
        SingletonClass.getInstance().showCircleScreen(Constants.filterId,
                this, binder.imgFilter, getResources().getString(R.string.filtermessage), 50, 0, new IShowcaseListener() {
                    @Override
                    public void onShowcaseDisplayed(MaterialShowcaseView materialShowcaseView) {

                    }

                    @Override
                    public void onShowcaseDismissed(MaterialShowcaseView materialShowcaseView) {
                        try {
                            SingletonClass.getInstance().showCircleScreen(Constants.seaerchId,
                                    HomePhoneActivity.this, binder.imgGlobalSearch, getResources().getString(R.string.searchmessage), 50, 0, new IShowcaseListener() {
                                        @Override
                                        public void onShowcaseDisplayed(MaterialShowcaseView materialShowcaseView) {

                                        }

                                        @Override
                                        public void onShowcaseDismissed(MaterialShowcaseView materialShowcaseView) {
                                            try {
                                                SingletonClass.getInstance().showRectangleScreen(Constants.scrollmoreId,
                                                        HomePhoneActivity.this, binder.pager, getResources().getString(R.string.scrollmoremessage),
                                                        0, new IShowcaseListener() {
                                                            @Override
                                                            public void onShowcaseDisplayed(MaterialShowcaseView materialShowcaseView) {

                                                            }

                                                            @Override
                                                            public void onShowcaseDismissed(MaterialShowcaseView materialShowcaseView) {

                                                            }
                                                        });
                                            } catch (Exception ignore) {
                                            }
                                        }
                                    });
                        } catch (Exception ignore) {
                        }
                    }
                });
    }

    private void setViews() {
        if (restaurantPagerAdapter == null) {
            restaurantPagerAdapter = new RestaurantPagerAdapter(getSupportFragmentManager());
        }
        binder.pager.setAdapter(restaurantPagerAdapter);
        binder.imgGlobalSearch.setVisibility(View.VISIBLE);
        binder.pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(final int position) {
                if (position == 0) {
                    binder.imgGlobalSearch.setVisibility(View.VISIBLE);
                    binder.imgFilter.setVisibility(View.VISIBLE);
                    if (binder.txtFilterCount.getText().toString().length() > 0) {
                        binder.txtFilterCount.setVisibility(View.VISIBLE);
                    }
                } else if (position == 1) {
                    binder.imgGlobalSearch.setVisibility(View.GONE);
                    binder.imgFilter.setVisibility(View.GONE);
                    binder.txtFilterCount.setVisibility(View.GONE);
                    if (binder.searchField.getVisibility() == View.VISIBLE) {
                        showSearchField(false, "");
                        searchEdtVisible = false;
                        removeQueryFilter();
                    }
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            RestaurantsList restaurantsList = (RestaurantsList) restaurantPagerAdapter.getFragment(position);
                            restaurantsList.refreshOfflineList();
                        }
                    }, 200);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        binder.tabLayout.setupWithViewPager(binder.pager);

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocationTracker.getInstance(this).stopFindingLocation();
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == binder.imgFilter.getId()) {
            if (filterModels == null || filterModels.size() <= 0) {
                getCuisineListAndMoveFurther();
            } else {
                goToFilterActivity();
            }
        } else if (view.getId() == binder.imgGlobalSearch.getId()) {
            if (searchEdtVisible) {
                showSearchField(false, "");
                searchEdtVisible = false;
                removeQueryFilter();
            } else {
                showSearchField(true, "");
                searchEdtVisible = true;
            }
        }
    }

    public void setFilterCount(int value) {
        if (value == 0) {
            binder.txtFilterCount.setText("");
            binder.txtFilterCount.setVisibility(View.GONE);
            return;
        }
        binder.txtFilterCount.setText(String.valueOf(value));
        binder.txtFilterCount.setVisibility(View.VISIBLE);
    }

    private void getCuisineListAndMoveFurther() {
        filterRequest = new CustomHashmap<>();
        filterRequest.put(Constants.lat, String.valueOf(SingletonClass.getInstance().getCurrentLat()));
        filterRequest.put(Constants.lon, String.valueOf(SingletonClass.getInstance().getCurrentLong()));
        filterRequest.put(Constants.city_id, "1");

        HttpClient httpClient = new HttpClient();
        httpClient.post(SingletonClass.getInstance().getBaseUrl() + "cuisines?" + filterRequest.toString(), this, null, true, new IHttpPostResult() {
            @Override
            public void onSuccess(JSONObject data) {
                CuisinesModel model = new Gson().fromJson(data.toString(), CuisinesModel.class);
                if (model == null || model.getCuisines() == null || model.getCuisines().size() <= 0) {
                    SingletonClass.getInstance().showSnackbar(binder.getRoot(), getResources().getString(R.string.Failed_to_filter_Restaurant_Please_try_again_later));
                    return;
                }
                filterModels = new ArrayList<>();

                MultiselectFilterModel multiselectFilterModel = new MultiselectFilterModel();
                multiselectFilterModel.setKey(getResources().getString(R.string.cuisines));
                ArrayList<MultiSelectFilterOption> arrayList = new ArrayList();
                for (int i = 0; i < model.getCuisines().size(); i++) {
                    arrayList.add(new MultiSelectFilterOption(model.getCuisines().get(i).getCuisine().getCuisineName(), false,
                            model.getCuisines().get(i).getCuisine().getCuisineId()));
                }
                multiselectFilterModel.setFilterParams(arrayList);
                filterModels.add(multiselectFilterModel);

                SingleSelectFilterModel singleSelectFilterModel = new SingleSelectFilterModel();
                singleSelectFilterModel.setKey(getResources().getString(R.string.sortby));
                ArrayList<SingleSelectFilterOption> singleSelectFilterOptionArrayList = new ArrayList<>();
                singleSelectFilterOptionArrayList.add(new SingleSelectFilterOption(getResources().getString(R.string.cost), false, "cost"));
                singleSelectFilterOptionArrayList.add(new SingleSelectFilterOption(getResources().getString(R.string.rating), false, "rating   "));
                singleSelectFilterOptionArrayList.add(new SingleSelectFilterOption(getResources().getString(R.string.distance), false, "real_distance"));

                singleSelectFilterModel.setFilterParams(singleSelectFilterOptionArrayList);
                filterModels.add(singleSelectFilterModel);

                goToFilterActivity();
            }
        });

    }

    private void goToFilterActivity() {
        Bundle extras = new Bundle();
        extras.putSerializable(Constants.FilterModel, filterModels);
        Intent intent = new Intent(HomePhoneActivity.this, CuisineFilterActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.putExtras(extras);
        startActivityForResult(intent, FILTER_ACTION);
    }

    @Override
    public void onBackPressed() {
        if (searchEdtVisible) {
            showSearchField(false, "");
            searchEdtVisible = false;
            removeQueryFilter();
        } else {
            super.onBackPressed();
        }
    }

    private void removeQueryFilter() {
        ArrayList<BaseFilterModel> baseFilterModels = new ArrayList<>();
        CustomModel customModel = new CustomModel();
        customModel.setFilterText("");

        baseFilterModels.add(customModel);
        Intent intent = new Intent();
        intent.putExtra(Constants.FilterModel, baseFilterModels);
        intent.setAction(Constants.RestaurantRefreshFilter);
        LocalBroadcastManager.getInstance(HomePhoneActivity.this).sendBroadcast(intent);
        SingletonClass.getInstance().hideKeyboard(binder.searchField);
    }

    protected void showSearchField(final boolean visible, final String searchText) {
        if (visible) {
            binder.searchField.setVisibility(View.VISIBLE);
            Animation animation = AnimationUtils.loadAnimation(this, R.anim.expand_from_center);
            animation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    binder.searchField.requestFocus();
                    if (!TextUtils.isEmpty(searchText)) {
                        binder.searchField.setText(searchText);
                        binder.searchField.setSelection(searchText.length());
                    } else {
                        SingletonClass.getInstance().showKeyboard(binder.searchField);
                    }
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            binder.searchField.startAnimation(animation);
        } else {
            binder.searchField.startAnimation(AnimationUtils.loadAnimation(this, R.anim.collapse_to_center));
            binder.searchField.setVisibility(View.GONE);
            binder.searchField.setText("");
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == FILTER_ACTION && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            if (extras != null) {
                filterModels = (ArrayList<BaseFilterModel>) extras.getSerializable(Constants.FilterModel);

                Intent intent = new Intent();
                intent.putExtra(Constants.FilterModel, filterModels);
                intent.setAction(Constants.RestaurantRefreshFilter);
                LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
            }
        }
    }

}
