package com.example.princebatra.princezomatotest.callbacks;


import com.example.princebatra.princezomatotest.common.CustomDialog;

public interface ITwoButtonDialogListener {
	
	void onPositiveClickListener(CustomDialog dialogInstance);
	void onNegativeClickListener(CustomDialog dialogInstance);

}
