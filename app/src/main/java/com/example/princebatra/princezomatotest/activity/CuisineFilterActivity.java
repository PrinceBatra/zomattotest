package com.example.princebatra.princezomatotest.activity;

import android.util.Log;

import com.example.princebatra.princezomatotest.activity.filter.BaseFilterActivity;
import com.example.princebatra.princezomatotest.common.Constants;
import com.example.princebatra.princezomatotest.models.filtermodel.BaseFilterModel;

import java.util.ArrayList;

/**
 * @author princebatra
 * @version 1.0
 * @since 6/6/17
 */
public class CuisineFilterActivity extends BaseFilterActivity {
    @Override
    public ArrayList<BaseFilterModel> getFilterOptionMap() {
        try {
            return (ArrayList<BaseFilterModel>) getIntent().getSerializableExtra(Constants.FilterModel);
        } catch (Exception e) {
            Log.e("prince", e.toString());
            return null;
        }
    }
}
