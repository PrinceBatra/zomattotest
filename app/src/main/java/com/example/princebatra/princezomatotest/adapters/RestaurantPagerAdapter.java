package com.example.princebatra.princezomatotest.adapters;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import com.example.princebatra.princezomatotest.common.Constants;
import com.example.princebatra.princezomatotest.fragments.RestaurantsList;

import java.util.ArrayList;

/**
 * @author princebatra
 * @version 1.0
 * @since 6/6/17
 */
public class RestaurantPagerAdapter extends FragmentStatePagerAdapter {

    private ArrayList<Fragment> fragmentQueue;

    public RestaurantPagerAdapter(FragmentManager fm) {
        super(fm);
        fragmentQueue = new ArrayList<>();
    }

    public Fragment getFragment(int position) {
        return fragmentQueue.get(position);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                RestaurantsList nearYouList = new RestaurantsList();
                Bundle bundle = new Bundle();
                bundle.putBoolean(Constants.fromWHere, false);
                nearYouList.setArguments(bundle);
                fragmentQueue.add(nearYouList);
                return nearYouList;
            case 1:
                RestaurantsList savedList = new RestaurantsList();
                Bundle savedbundle = new Bundle();
                savedbundle.putBoolean(Constants.fromWHere, true);
                savedList.setArguments(savedbundle);
                fragmentQueue.add(savedList);
                return savedList;
            default:
                return null;
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String title = null;
        if (position == 0) {
            title = "Near You";
        } else if (position == 1) {
            title = "Offline";
        }
        return title;
    }

    @Override
    public int getCount() {
        return 2;
    }
}
