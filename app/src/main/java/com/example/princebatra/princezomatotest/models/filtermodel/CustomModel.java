package com.example.princebatra.princezomatotest.models.filtermodel;

/**
 * @author princebatra
 * @version 1.0
 * @since 6/6/17
 */
public class CustomModel extends BaseFilterModel {


    private String filterText;

    @Override
    public String getLeftSideKey() {
        return null;
    }

    @Override
    public String getFragmentClassName() {
        return null;
    }

    public String getFilterText() {
        return filterText;
    }

    public void setFilterText(String filterText) {
        this.filterText = filterText;
    }
}
