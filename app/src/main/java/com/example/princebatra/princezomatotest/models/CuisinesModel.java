package com.example.princebatra.princezomatotest.models;


import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author princebatra
 * @version 1.0
 * @since 6/6/17
 */
public class CuisinesModel {
    @SerializedName("cuisines")
    @Expose
    private List<Cuisine> cuisines = null;

    public List<Cuisine> getCuisines() {
        return cuisines;
    }

    public void setCuisines(List<Cuisine> cuisines) {
        this.cuisines = cuisines;
    }

    public class Cuisine {

        @SerializedName("cuisine_id")
        @Expose
        private Integer cuisineId;
        @SerializedName("cuisine_name")
        @Expose
        private String cuisineName;

        @SerializedName("cuisine")
        @Expose
        private Cuisine cuisine;

        public Integer getCuisineId() {
            return cuisineId;
        }

        public void setCuisineId(Integer cuisineId) {
            this.cuisineId = cuisineId;
        }

        public String getCuisineName() {
            return cuisineName;
        }

        public void setCuisineName(String cuisineName) {
            this.cuisineName = cuisineName;
        }

        public Cuisine getCuisine() {
            return cuisine;
        }

        public void setCuisine(Cuisine cuisine) {
            this.cuisine = cuisine;
        }
    }

}
