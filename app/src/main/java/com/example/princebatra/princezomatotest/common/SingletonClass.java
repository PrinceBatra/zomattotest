package com.example.princebatra.princezomatotest.common;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.example.princebatra.princezomatotest.R;
import com.example.princebatra.princezomatotest.utils.CustomCircle;
import com.example.princebatra.princezomatotest.utils.CustomMaterialShowcaseViewBuilder;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.TimerTask;

import uk.co.deanwild.materialshowcaseview.IShowcaseListener;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseView;

/**
 * @author princebatra
 * @version 1.0
 * @since 6/6/17
 */
public class SingletonClass {

    private static SingletonClass singletonClass;
    private Double currentLat;
    private Double currentLong;
    private SharedPreferences pref;
    private SharedPreferences.Editor editPref;
    private Toast m_currentToast;

    public void showCircleScreen(final String id, final Activity activity, final View view, final String message,
                                        final int radius, int delay, final IShowcaseListener listener) {
        new Handler().postDelayed(new TimerTask() {
            @Override
            public void run() {
                new CustomMaterialShowcaseViewBuilder(activity)
                        .setTarget(view)
                        .setDismissText("")
                        .setTargetTouchable(false)
                        .setDismissOnTouch(true)
                        .setShape(new CustomCircle(radius))
                        .setContentText(message)
                        .singleUse(id)
                        .setMaskColour(ContextCompat.getColor(activity, R.color.black_85))
                        .setListener(listener != null ? listener : new IShowcaseListener() {
                            @Override
                            public void onShowcaseDisplayed(MaterialShowcaseView materialShowcaseView) {

                            }

                            @Override
                            public void onShowcaseDismissed(MaterialShowcaseView materialShowcaseView) {

                            }
                        })
                        .show();
            }
        }, delay);
    }

    public void showRectangleScreen(final String id, final Activity activity, final View view, final String message,
                                           int delay, final IShowcaseListener listener) {
        new Handler().postDelayed(new TimerTask() {
            @Override
            public void run() {
                new CustomMaterialShowcaseViewBuilder(activity)
                        .setTarget(view)
                        .setDismissText("")
                        .setTargetTouchable(false)
                        .setDismissOnTouch(true)
                        .withRectangleShape()
                        .setContentText(message)
                        .setShapePadding(0)
                        .singleUse(id)
                        .setMaskColour(ContextCompat.getColor(activity, R.color.black_85))
                        .setListener(listener != null ? listener : new IShowcaseListener() {
                            @Override
                            public void onShowcaseDisplayed(MaterialShowcaseView materialShowcaseView) {

                            }

                            @Override
                            public void onShowcaseDismissed(MaterialShowcaseView materialShowcaseView) {

                            }
                        })
                        .show();
            }
        }, delay);
    }



    public void setLatLong(Context context, Double _currentLat, Double _currentLong) {
        currentLat = _currentLat;
        currentLong = _currentLong;
    }

    public void showKeyboard(View view) {
        if (view == null) {
            return;
        }
        getInputMethodManager(view.getContext()).showSoftInput(view, InputMethodManager.SHOW_FORCED);
    }

    private InputMethodManager getInputMethodManager(Context context) {
        return (InputMethodManager)
                context.getSystemService(Context.INPUT_METHOD_SERVICE);
    }



    public void hideKeyboard(View view) {
        if (view == null) {
            return;
        }
        Context context = view.getContext();

        getInputMethodManager(context).hideSoftInputFromWindow(view.getWindowToken(),
                InputMethodManager.RESULT_UNCHANGED_SHOWN);
    }


    public Double getCurrentLong() {
        return currentLong;
    }

    public Double getCurrentLat() {
        return currentLat;
    }

    public String getDistance(Double lati, Double longi) {
        try {
            android.location.Location currentLocation = new android.location.Location("CurrentLocation");
            currentLocation.setLatitude(currentLat);
            currentLocation.setLongitude(currentLong);

           /* Double destinationLat = result.getGeometry().getLocation().getLat();
            Double destinationLong = result.getGeometry().getLocation().getLng();
*/
            android.location.Location destinationLocation = new android.location.Location("DestinationLocation");
            destinationLocation.setLatitude(lati);
            destinationLocation.setLongitude(longi);

            float distanceFloat = currentLocation.distanceTo(destinationLocation);
            // float distanceFloat = distanceFrom(currentLat,currentLon,lati,longi);


            return String.valueOf(distanceFloat);
        } catch (Exception e) {
            Log.d("tag", e.toString());
            return null;
        }

    }



    public boolean isNetworkAvailable(Context act) {
        try {
            if (act == null) {
                return false;
            }
            ConnectivityManager connectivity = (ConnectivityManager) act.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (connectivity != null) {
                NetworkInfo activeNetwork = connectivity.getActiveNetworkInfo();
                return activeNetwork != null && (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI || activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE);
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }

    }


    public synchronized static SingletonClass getInstance() {
        if (singletonClass == null) {
            singletonClass = new SingletonClass();
        }
        return singletonClass;
    }

    public void showSnackbar(View view, String msg) {
        Snackbar.make(view, msg, Snackbar.LENGTH_LONG).setAction("Ok", new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        }).show();
    }

    public String getBaseUrl() {
        return "https://developers.zomato.com/api/v2.1/";
    }

    public void showToast(String msg, Context activity) {
        if (activity == null) {
            return;
        }
        if (m_currentToast != null) {
            m_currentToast.cancel();
        }
        m_currentToast = Toast.makeText(activity, msg, Toast.LENGTH_SHORT);
        m_currentToast.show();
    }

    public void storeInformation(String key, String val, Context context) {
        try {
            if (editPref == null) {
                prepareNecessaryInstance(context);
            }
            editPref.putString(key, val);
            editPref.commit();
        } catch (Exception e) {
        }

    }

    public void storeInformation(String key, int val, Context context) {
        try {
            if (editPref == null) {
                prepareNecessaryInstance(context);
            }
            editPref.putInt(key, val);
            editPref.commit();
        } catch (Exception e) {
        }
    }

    public void storeInformation(String key, boolean val, Context context) {
        try {
            if (editPref == null) {
                prepareNecessaryInstance(context);
            }
            editPref.putBoolean(key, val);
            editPref.commit();
        } catch (Exception e) {
        }
    }

    public void clearSharePreference(Context context) {
        try {
            if (editPref == null) {
                prepareNecessaryInstance(context);
            }
            editPref.clear();
            editPref.commit();
        } catch (Exception e) {
        }
    }

    public void remove(String key, Context context) {
        try {
            if (editPref == null) {
                prepareNecessaryInstance(context);
            }
            editPref.remove(key);
            editPref.commit();
        } catch (Exception e) {
        }
    }

    public String getString(String key, Context context) {
        try {
            if (pref == null) {
                prepareNecessaryInstance(context);
            }

            return pref.getString(key, null);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * It return 0 if no value found to particular key.
     */
    synchronized public int getInteger(String key, Context context) {
        try {
            if (pref == null) {
                prepareNecessaryInstance(context);
            }
            return pref.getInt(key, 0);
        } catch (Exception e) {
            return 0;
        }
    }

    /**
     * It return false if no value found to particular key.
     */
    synchronized public boolean getBoolean(String key, boolean defaultValue, Context context) {
        try {
            if (pref == null) {
                prepareNecessaryInstance(context);
            }
            return pref.getBoolean(key, defaultValue);
        } catch (Exception e) {
            return defaultValue;
        }
    }


    synchronized public void prepareNecessaryInstance(Context context) {
        try {
            if (context == null) {
                return;
            }
            pref = context.getSharedPreferences("LocationTracker", Context.MODE_PRIVATE);
            editPref = pref.edit();
            editPref.commit();
        } catch (Exception e) {
        }
    }


}
