package com.example.princebatra.princezomatotest.common;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import com.example.princebatra.princezomatotest.R;
import com.example.princebatra.princezomatotest.callbacks.ISingleButtonDialogListener;
import com.example.princebatra.princezomatotest.callbacks.ITwoButtonDialogListener;


public class CustomDialog extends Dialog implements DialogInterface.OnDismissListener {

    private Context _activity;


    public CustomDialog(Context context, int progress) {
        super(context, R.style.NewDialog);
        this._activity = context;
        setOnDismissListener(this);
    }


    public void displaySingleButtonDailog(String message, String buttonText, final ISingleButtonDialogListener callback) {
        try {
            final AlertDialog.Builder alertDialog = new AlertDialog.Builder(_activity);
            alertDialog.setTitle("Alert");
            alertDialog.setMessage(message);
            alertDialog.setCancelable(false);
            alertDialog.setIcon(android.R.drawable.ic_dialog_alert);


            alertDialog.setPositiveButton(buttonText,
                    new OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            callback.onPositiveClickListener(CustomDialog.this);
                        }
                    });

            alertDialog.show();
        } catch (Exception e) {

        }
    }

    public void displayTwoButtonsDialog(String message, String positiveButtonText, String negativeButtonText, final ITwoButtonDialogListener callback) {

        try {
            final AlertDialog.Builder alertDialog = new AlertDialog.Builder(_activity);
            alertDialog.setTitle("Alert");
            alertDialog.setMessage(message);
            alertDialog.setCancelable(false);
            alertDialog.setIcon(android.R.drawable.ic_dialog_alert);

            alertDialog.setPositiveButton(positiveButtonText,
                    new OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            callback.onPositiveClickListener(CustomDialog.this);
                        }
                    });


            alertDialog.setNegativeButton(negativeButtonText,
                    new OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            callback.onNegativeClickListener(CustomDialog.this);
                        }
                    });
            alertDialog.show();

        } catch (Exception e) {
            Log.e("tag", e.toString());
        }
    }

    private void hideKeypad() {
        try {
            final View view = getCurrentFocus();
            if (view != null) {
                InputMethodManager imm = (InputMethodManager) _activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            }
        } catch (Exception e) {
            Log.e("tag", e.toString());
        }
    }

    public void displayUiBlockingDialog() {
        try {
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            setContentView(R.layout.ui_alert_dialog);
            setCanceledOnTouchOutside(false);
            setCancelable(false);
            show();
        } catch (Exception e) {

        }
    }


    public void stopUiBlocking() {
        dismiss();
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        try {
            hideKeypad();
        } catch (Exception e) {

        }
    }
}
