package com.example.princebatra.princezomatotest.utils;

import java.util.HashMap;
import java.util.Iterator;

/**
 * @author princebatra
 * @version 1.0
 * @since 6/6/17
 */
public class CustomHashmap<K,V> extends HashMap {


    @Override
    public String toString() {
        Iterator<Entry<K,V>> i = entrySet().iterator();
        if (! i.hasNext())
            return "";

        StringBuilder sb = new StringBuilder();
        for (;;) {
            Entry<K,V> e = i.next();
            K key = e.getKey();
            V value = e.getValue();
            sb.append(key   == this ? "(this Map)" : key);
            sb.append('=');
            sb.append(value == this ? "(this Map)" : value);
            if (! i.hasNext())
                return sb.toString();
            sb.append('&');
        }
    }
}
