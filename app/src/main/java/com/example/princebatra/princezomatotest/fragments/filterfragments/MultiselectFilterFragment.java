package com.example.princebatra.princezomatotest.fragments.filterfragments;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ListView;

import com.example.princebatra.princezomatotest.R;
import com.example.princebatra.princezomatotest.callbacks.filtercallbacks.OnFilterItemClicked;
import com.example.princebatra.princezomatotest.common.Constants;
import com.example.princebatra.princezomatotest.databinding.ListFilterRowBinding;
import com.example.princebatra.princezomatotest.models.filtermodel.BaseFilterModel;
import com.example.princebatra.princezomatotest.models.filtermodel.MultiselectFilterModel;

/**
 * @author princebatra
 * @version 1.0
 * @since 6/6/17
 */
public class MultiselectFilterFragment extends BaseFilterFragment implements AdapterView.OnItemClickListener {

    private ListView listView;
    private BaseAdapter adapter;
    private OnFilterItemClicked listener;
    private MultiselectFilterModel multiselectFilterModel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        multiselectFilterModel = (MultiselectFilterModel) getArguments().getSerializable(Constants.FilterModel);
        return inflater.inflate(R.layout.fragment_filter, container, false);
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        listView = (ListView) view.findViewById(R.id.listView);
        EditText searchEditText = (EditText) view.findViewById(R.id.searchEditText);
        searchEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                EditText editText = (EditText) v;
                if (hasFocus && !editText.getText().toString().trim().isEmpty()) {
                    editText.setCompoundDrawablesWithIntrinsicBounds(0, 0, android.R.drawable.ic_menu_close_clear_cancel, 0);
                } else if (!hasFocus && editText.getText().toString().trim().isEmpty()) {
                    editText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_search, 0);
                }
            }
        });
        searchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
               /* if (!filterOptions) {
                    // ((MultiselectFilterFragment.FilterDataAdapter) adapter).getFilter().filter(s);
                }*/
            }
        });
        adapter = new FilterDataAdapter();
        listView.setAdapter(this.adapter);
        listView.setOnItemClickListener(this);
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        try {
            ListFilterRowBinding binder = DataBindingUtil.getBinding(view);
            if (multiselectFilterModel.filterParams.get(position).isSelected()) {
                multiselectFilterModel.filterParams.get(position).setSelected(false);
                binder.dataTick.setVisibility(View.GONE);
                binder.data.setTextColor(ContextCompat.getColor(getContext(), R.color.gray));
            } else {
                multiselectFilterModel.filterParams.get(position).setSelected(true);
                binder.data.setTextColor(ContextCompat.getColor(getContext(), R.color.colorPrimary));
                binder.dataTick.setVisibility(View.VISIBLE);
            }

        } catch (Exception ex) {
        }
    }


    @Override
    public BaseFilterModel getModel() {
        return multiselectFilterModel;
    }

    @Override
    public void resetCommandReceived() {
        if (multiselectFilterModel.filterParams != null &&
                multiselectFilterModel.filterParams.size() > 0) {
            for (int i = 0; i < multiselectFilterModel.filterParams.size(); i++) {
                multiselectFilterModel.filterParams.get(i).setSelected(false);
            }
            adapter.notifyDataSetChanged();
        }
    }


    private class FilterDataAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return multiselectFilterModel.filterParams.size();
        }

        @Override
        public Object getItem(int position) {
            return multiselectFilterModel.filterParams.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ListFilterRowBinding binder = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                    R.layout.list_filter_row, parent, false);

            binder.data.setText((multiselectFilterModel.filterParams.get(position)).getOptionValue());

            if (multiselectFilterModel.filterParams.get(position).isSelected()) {
                binder.data.setTextColor(ContextCompat.getColor(getContext(), R.color.colorPrimary));
                binder.dataTick.setVisibility(View.VISIBLE);
            } else {
                binder.dataTick.setVisibility(View.GONE);
                binder.data.setTextColor(ContextCompat.getColor(getContext(), R.color.gray));
            }
            return binder.getRoot();
        }

    }

}
