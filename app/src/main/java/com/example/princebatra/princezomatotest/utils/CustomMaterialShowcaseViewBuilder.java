package com.example.princebatra.princezomatotest.utils;

import android.app.Activity;
import android.util.TypedValue;
import android.widget.TextView;

import com.example.princebatra.princezomatotest.R;

import uk.co.deanwild.materialshowcaseview.MaterialShowcaseView;

/**
 * @author princebatra
 * @version 1.0
 * @since 6/6/17
 */
public class CustomMaterialShowcaseViewBuilder extends MaterialShowcaseView.Builder {

    public CustomMaterialShowcaseViewBuilder(Activity activity) {
        super(activity);
    }

    @Override
    public MaterialShowcaseView build() {
        MaterialShowcaseView view = super.build();
        TextView textView = (TextView) view.findViewById(R.id.tv_content);
        if (textView != null) {
            textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
        }
        return view;
    }


}
