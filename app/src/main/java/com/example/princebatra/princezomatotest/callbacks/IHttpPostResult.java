package com.example.princebatra.princezomatotest.callbacks;

import org.json.JSONObject;

public interface IHttpPostResult {
	
	void onSuccess(JSONObject data);

}
