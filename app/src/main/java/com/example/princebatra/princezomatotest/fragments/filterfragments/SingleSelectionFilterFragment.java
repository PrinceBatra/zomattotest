package com.example.princebatra.princezomatotest.fragments.filterfragments;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ListView;

import com.example.princebatra.princezomatotest.R;
import com.example.princebatra.princezomatotest.common.Constants;
import com.example.princebatra.princezomatotest.databinding.ListFilterRowBinding;
import com.example.princebatra.princezomatotest.databinding.SingleselectFilterRowBinding;
import com.example.princebatra.princezomatotest.models.filtermodel.BaseFilterModel;
import com.example.princebatra.princezomatotest.models.filtermodel.MultiselectFilterModel;
import com.example.princebatra.princezomatotest.models.filtermodel.SingleSelectFilterModel;
import com.example.princebatra.princezomatotest.models.filtermodel.SingleSelectFilterOption;

/**
 * @author princebatra
 * @version 1.0
 * @since 6/6/17
 */
public class SingleSelectionFilterFragment extends BaseFilterFragment implements AdapterView.OnItemClickListener {

    private SingleSelectFilterModel singleSelectFilterModel;
    private ListView listView;
    private BaseAdapter adapter;
    private View previousView;
    private int previousPosition;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        singleSelectFilterModel = (SingleSelectFilterModel) getArguments().getSerializable(Constants.FilterModel);
        return inflater.inflate(R.layout.fragment_filter, container, false);
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        listView = (ListView) view.findViewById(R.id.listView);
        EditText searchEditText = (EditText) view.findViewById(R.id.searchEditText);
        searchEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                EditText editText = (EditText) v;
                if (hasFocus && !editText.getText().toString().trim().isEmpty()) {
                    editText.setCompoundDrawablesWithIntrinsicBounds(0, 0, android.R.drawable.ic_menu_close_clear_cancel, 0);
                } else if (!hasFocus && editText.getText().toString().trim().isEmpty()) {
                    editText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_search, 0);
                }
            }
        });
        searchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
               /* if (!filterOptions) {
                    // ((MultiselectFilterFragment.FilterDataAdapter) adapter).getFilter().filter(s);
                }*/
            }
        });
        adapter = new FilterDataAdapter();
        listView.setAdapter(this.adapter);
        listView.setOnItemClickListener(this);
    }


    @Override
    public BaseFilterModel getModel() {
        return singleSelectFilterModel;
    }

    @Override
    public void resetCommandReceived() {
        if (singleSelectFilterModel.filterParams != null &&
                singleSelectFilterModel.filterParams.size() > 0) {
            for (int i = 0; i < singleSelectFilterModel.filterParams.size(); i++) {
                singleSelectFilterModel.filterParams.get(i).setSelected(false);
            }
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        try {
            if (previousView != null) {
                SingleselectFilterRowBinding previouseBinder = DataBindingUtil.getBinding(previousView);
                previouseBinder.checked.setChecked(false);
                previouseBinder.data.setTextColor(ContextCompat.getColor(getContext(), R.color.gray));
                singleSelectFilterModel.filterParams.get(previousPosition).setSelected(false);
            }

            SingleselectFilterRowBinding newBinder = DataBindingUtil.getBinding(view);
            newBinder.data.setTextColor(ContextCompat.getColor(getContext(), R.color.colorPrimary));
            newBinder.checked.setChecked(true);
            singleSelectFilterModel.filterParams.get(position).setSelected(true);
            previousPosition = position;
            previousView = view;

            adapter.notifyDataSetChanged();

        } catch (Exception ignored) {
        }
    }


    private class FilterDataAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return singleSelectFilterModel.filterParams.size();
        }

        @Override
        public Object getItem(int position) {
            return singleSelectFilterModel.filterParams.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            SingleselectFilterRowBinding binder = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                    R.layout.singleselect_filter_row, parent, false);

            binder.data.setText((singleSelectFilterModel.filterParams.get(position)).getOptionValue());

            if (singleSelectFilterModel.filterParams.get(position).isSelected()) {
                binder.data.setTextColor(ContextCompat.getColor(getContext(), R.color.colorPrimary));
                binder.checked.setChecked(true);
                previousPosition = position;
                previousView = binder.getRoot();
            } else {
                binder.checked.setChecked(false);
                binder.data.setTextColor(ContextCompat.getColor(getContext(), R.color.gray));
            }
            return binder.getRoot();
        }

    }
}
