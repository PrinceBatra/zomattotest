package com.example.princebatra.princezomatotest.models.filtermodel;

import java.io.Serializable;

/**
 * @author princebatra
 * @version 1.0
 * @since 6/6/17
 */
public class MultiSelectFilterOption implements Serializable {
    private String optionValue;
    private int optionId;
    private boolean isSelected;

    public MultiSelectFilterOption() {
    }

    public MultiSelectFilterOption(String optionValue, boolean isSelected) {
        this.optionValue = optionValue;
        this.isSelected = isSelected;
    }

    public MultiSelectFilterOption(String optionValue, boolean isSelected, int _optionId) {
        this.optionValue = optionValue;
        this.isSelected = isSelected;
        this.optionId = _optionId;
    }


    public String getOptionValue() {
        return optionValue;
    }

    public void setOptionValue(String optionValue) {
        this.optionValue = optionValue;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {

        isSelected = selected;
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof String && obj.equals(optionValue);
    }

    public int getOptionId() {
        return optionId;
    }

    public void setOptionId(int optionId) {
        this.optionId = optionId;
    }
}
