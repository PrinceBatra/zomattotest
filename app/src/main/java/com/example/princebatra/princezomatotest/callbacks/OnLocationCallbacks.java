package com.example.princebatra.princezomatotest.callbacks;


import com.example.princebatra.princezomatotest.common.Status;

/**
 * @author princebatra
 * @version 1.0
 * @since 6/6/17
 */
public interface OnLocationCallbacks {

    void onComplete(Status status);
}
