package com.example.princebatra.princezomatotest.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.princebatra.princezomatotest.R;
import com.example.princebatra.princezomatotest.activity.HomePhoneActivity;
import com.example.princebatra.princezomatotest.adapters.RestaurantsAdapter;
import com.example.princebatra.princezomatotest.callbacks.IHttpPostResult;
import com.example.princebatra.princezomatotest.common.Constants;
import com.example.princebatra.princezomatotest.dal.DatabaseHandler;
import com.example.princebatra.princezomatotest.utils.CustomHashmap;
import com.example.princebatra.princezomatotest.common.HttpClient;
import com.example.princebatra.princezomatotest.common.SingletonClass;
import com.example.princebatra.princezomatotest.databinding.RestaurantListBinding;
import com.example.princebatra.princezomatotest.models.RestaurantsModel;
import com.example.princebatra.princezomatotest.models.filtermodel.BaseFilterModel;
import com.example.princebatra.princezomatotest.models.filtermodel.CustomModel;
import com.example.princebatra.princezomatotest.models.filtermodel.MultiselectFilterModel;
import com.example.princebatra.princezomatotest.models.filtermodel.SingleSelectFilterModel;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * @author princebatra
 * @version 1.0
 * @since 6/6/17
 */
public class RestaurantsList extends Fragment {

    private RestaurantListBinding binder;
    private LinearLayoutManager mLayoutManager;
    private boolean mIsLoading, stopSearching;
    private int pastVisiblesItems, visibleItemCount, totalItemCount, offset;
    private RestaurantsAdapter adapter;
    private CustomHashmap<String, String> zomatoRequest = new CustomHashmap();
    private List<RestaurantsModel.Restaurant> restaurants;
    private ArrayList<BaseFilterModel> filterModels;
    private int filterCount;
    private boolean fromOffline;


    private BroadcastReceiver restaurantRefreshReciever = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            filterCount = 0;
            filterModels = (ArrayList<BaseFilterModel>) intent.getExtras().getSerializable(Constants.FilterModel);
            if (filterModels != null && filterModels.size() > 0) {
                clearRestaurantList();
                for (BaseFilterModel baseFilterModel : filterModels) {
                    if (baseFilterModel instanceof MultiselectFilterModel) {
                        addCuisinesFilter(baseFilterModel);
                    } else if (baseFilterModel instanceof SingleSelectFilterModel) {
                        addSortingFilter(baseFilterModel);
                    } else if (baseFilterModel instanceof CustomModel) {
                        addQueryFilter(baseFilterModel);
                    }
                }
                goForRestaurantsSearch();
            }
            HomePhoneActivity homePhoneActivity = (HomePhoneActivity) getActivity();
            homePhoneActivity.setFilterCount(filterCount);
        }
    };

    private void addQueryFilter(BaseFilterModel baseFilterModel) {
        CustomModel customModel = (CustomModel) baseFilterModel;
        if (customModel.getFilterText() != null && customModel.getFilterText().length() > 0) {
            zomatoRequest.put(Constants.query, customModel.getFilterText());
        } else {
            zomatoRequest.remove(Constants.query);
        }
    }

    private void clearRestaurantList() {
        offset = 0;
        zomatoRequest.clear();
        adapter = null;
        restaurants = null;
    }

    private void addSortingFilter(BaseFilterModel baseFilterModel) {
        SingleSelectFilterModel singleSelectFilterModel = (SingleSelectFilterModel) baseFilterModel;
        if (singleSelectFilterModel.filterParams != null && singleSelectFilterModel.filterParams.size() > 0) {
            for (int i = 0; i < singleSelectFilterModel.filterParams.size(); i++) {
                if (singleSelectFilterModel.filterParams.get(i).isSelected()) {
                    filterCount++;
                    zomatoRequest.put(Constants.sort, singleSelectFilterModel.filterParams.get(i).getOptionId());
                    zomatoRequest.put(Constants.order, "asc");
                    break;
                }
            }
        }
    }

    private void addCuisinesFilter(BaseFilterModel baseFilterModel) {
        boolean filterCountAlreadIncreased = false;
        MultiselectFilterModel multiselectFilterModel = (MultiselectFilterModel) baseFilterModel;
        if (multiselectFilterModel.filterParams != null && multiselectFilterModel.filterParams.size() > 0) {
            StringBuilder stringBuilder = new StringBuilder("");
            for (int i = 0; i < multiselectFilterModel.filterParams.size(); i++) {
                if (multiselectFilterModel.filterParams.get(i).isSelected()) {
                    if (!filterCountAlreadIncreased) {
                        filterCount++;
                        filterCountAlreadIncreased = true;
                    }
                    stringBuilder.append(String.valueOf(multiselectFilterModel.filterParams.get(i).getOptionId()));
                    stringBuilder.append(",");
                }
            }
            String cusisinesIds = stringBuilder.toString();
            if (cusisinesIds.length() > 0) {
                cusisinesIds = cusisinesIds.substring(0, cusisinesIds.length() - 1);
                zomatoRequest.put(Constants.cuisinesIds, cusisinesIds);
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(getActivity())
                .unregisterReceiver(restaurantRefreshReciever);
    }


    RecyclerView.OnScrollListener mScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            if (mIsLoading || stopSearching || fromOffline)
                return;
            if (dy > 0) //check for scroll down
            {
                visibleItemCount = mLayoutManager.getChildCount();
                totalItemCount = mLayoutManager.getItemCount();
                pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();
                if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                    offset = offset + 20;
                    goForRestaurantsSearch();
                    SingletonClass.getInstance().showToast("Going for more results", getActivity());
                }
            }
        }
    };


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binder = DataBindingUtil.inflate(inflater, R.layout.restaurant_list, container, false);
        mLayoutManager = new LinearLayoutManager(getActivity());
        binder.restaurantsRecyclerView.setLayoutManager(mLayoutManager);
        if(!fromOffline) {
            binder.restaurantsRecyclerView.addOnScrollListener(mScrollListener);
        }
        fromOffline = getArguments().getBoolean(Constants.fromWHere);
        if (!fromOffline) {
            LocalBroadcastManager.getInstance(getActivity()).registerReceiver(restaurantRefreshReciever,
                    new IntentFilter(Constants.RestaurantRefreshFilter));
        }
        if (adapter != null) {
            binder.restaurantsRecyclerView.setAdapter(adapter);
        } else {
            if (!fromOffline) {
                goForRestaurantsSearch();
            }
        }

        return binder.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshOfflineList();
    }

    public void refreshOfflineList() {
        if(fromOffline){
            restaurants = new DatabaseHandler(getActivity()).getRestaurants(null);
            if(restaurants == null || restaurants.size()<=0){
                binder.restaurantsRecyclerView.setVisibility(View.GONE);
                binder.txtNoRestro.setVisibility(View.VISIBLE);
                binder.txtNoRestro.setText(getActivity().getResources().getString(R.string.noofflinerestro));
            }else{
                adapter = new RestaurantsAdapter(restaurants, (AppCompatActivity) getActivity(),fromOffline);
                binder.restaurantsRecyclerView.setAdapter(adapter);
                binder.restaurantsRecyclerView.setVisibility(View.VISIBLE);
                binder.txtNoRestro.setVisibility(View.GONE);
            }
        }
    }

    private void goForRestaurantsSearch() {
        zomatoRequest.put(Constants.lat, String.valueOf(SingletonClass.getInstance().getCurrentLat()));
        zomatoRequest.put(Constants.lon, String.valueOf(SingletonClass.getInstance().getCurrentLong()));
        zomatoRequest.put(Constants.radius, "10000");
        zomatoRequest.put(Constants.start, offset);
        zomatoRequest.put(Constants.count, Constants.maxCount);


        binder.progressbar.setVisibility(View.VISIBLE);
        mIsLoading = true;
        HttpClient httpClient = new HttpClient();
        httpClient.post(SingletonClass.getInstance().getBaseUrl() + "search?" + zomatoRequest.toString(), getActivity(), null, false, new IHttpPostResult() {
            @Override
            public void onSuccess(JSONObject data) {
                binder.progressbar.setVisibility(View.GONE);
                mIsLoading = false;
                RestaurantsModel model = new Gson().fromJson(data.toString(), RestaurantsModel.class);
                if (model == null) {
                    SingletonClass.getInstance().showSnackbar(binder.getRoot(), "Failed to get restaruant.Please try agian later");
                    return;
                }
                if (adapter == null) {
                    restaurants = model.getRestaurants();
                    if (restaurants == null || restaurants.size() <= 0) {
                        binder.restaurantsRecyclerView.setVisibility(View.GONE);
                        binder.txtNoRestro.setVisibility(View.VISIBLE);
                        SingletonClass.getInstance().showSnackbar(binder.getRoot(), "No restaurants found.");
                        return;
                    }
                    adapter = new RestaurantsAdapter(restaurants, (AppCompatActivity) getActivity(),fromOffline);
                    binder.restaurantsRecyclerView.setAdapter(adapter);
                    binder.restaurantsRecyclerView.setVisibility(View.VISIBLE);
                    binder.txtNoRestro.setVisibility(View.GONE);
                } else {
                    addMoreRestaurants(model);
                    adapter.notifyDataSetChanged();
                    binder.restaurantsRecyclerView.setVisibility(View.VISIBLE);
                    binder.txtNoRestro.setVisibility(View.GONE);
                }

            }
        });
    }

    private void addMoreRestaurants(RestaurantsModel localModel) {
        List<RestaurantsModel.Restaurant> localRestro = localModel.getRestaurants();
        if (localRestro == null || localRestro.size() <= 0) {
            stopSearching = true;
            if (restaurants == null || restaurants.size() <= 0) {
                binder.restaurantsRecyclerView.setVisibility(View.GONE);
                binder.txtNoRestro.setVisibility(View.VISIBLE);
                SingletonClass.getInstance().showSnackbar(binder.getRoot(), "No restaurants found.");
            }
            return;
        }
        for (RestaurantsModel.Restaurant singleRestaurant : localRestro) {
            restaurants.add(singleRestaurant);
        }

    }


}
